==================================

This is the supplementary material
for Diploma scientific work about
developing multiple sequence alignment
program for transmembrane proteins
AlignTMP.

==================================

List of files:

#1 - "DOCUMENTATION_AlignTMP.txt"

Detailed and thorough documentation of the program.

#2 - "AlignTMP_jar"

Folder with the jar file of AlignTMP program

#3 - "NewMatrix"

Folder with new 210 calculated substituion matrices (based on Pfam statistics)
for all pairs of amino acids. Each file contains values for compairing pairs of residues
with different parameter (probability to be in membrane). For example, line from 
"AC.txt" file:

"AC	0.1	1.0	-23"

It means that if you want to align alanine with parameter between 0.0-0.1
(probability of being membrane) and cysteine with parameter between 0.9-1.0
you should use value -23 as a score of this pair.

#4 - "MEMBR_PFAM_397.txt"

List of Pfam domains, which were used for calculating new substitution matrices.

#5 - "MEMBR_PFAM_101.txt"

List of Pfam domains, which were not directly used for building new matrices.
(However, new data show that taking into account these domains do not change resulting
values significantly). 

 